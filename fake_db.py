"""
    Datastructures
"""

class FakeDB():
    """
       DB function
    """
    def __init__(self):
        self._db = {}

    def add_book(self, title, author):
        """ Add book function"""
        self._db[title] = (author)

    def check_book(self, title, author):
        """ Check if book exists function"""
        try:
            return self._db[title][0] == author
        except KeyError:
            return False

    def get_book(self, title):
        """ Get book function"""
        return self._db[title]

    # update author
    def update_book(self, title, new_author):
        """ Update book function"""
        try:
            self._db[title] = (new_author)
            return self._db[title]
        except KeyError:
            return False

    def delete_book(self, title):
        """ Delete a book function"""
        try:
            del self._db[title]
            return {"message": "deleted"}

        except KeyError:
            return False
