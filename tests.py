
"""
Test file
"""


# to send end point
from flask import json
from flask.testing import FlaskClient

import pytest
from app import create_app
from app import FakeDB



@pytest.fixture
def __app():
    """ App """
    _app_ = create_app()
    return _app_


@pytest.fixture
def app__():
    """App"""
    class TestClient(FlaskClient):
        """ Test client class"""
        def open(self, *args, **kwargs):
            """CLient """
            if 'json' in kwargs:
                kwargs['data'] = json.dumps(kwargs.pop('json'))
                kwargs['content_type'] = 'application/json'
            return super(TestClient, self).open(*args, **kwargs)

    app_ = create_app()
    # app.response_class = Response
    app_.test_client_class = TestClient
    app_.testing = True
    return app_



@pytest.fixture
def _client(__app):
    """Client"""
    return __app.test_client()

@pytest.fixture
def d_b():
    """ test db """
    return FakeDB()


def test_invalid_post(_client):
    """Test validity"""
    res = _client.post('/post', json={'title': 'TestT', 'author': 'test auth'})
    assert res.status_code == 404

def test_correct_post(_client):
    """Get correct posts"""
    title = "Test book"
    author = "Michael A"
    res = _client.post('/add_book', json={'title': title, 'author': author})
    assert res.status_code == 201

def test_get_book(_client, __app):
    """Get a book """
    title = "test_book"
    __app.db.add_book(title, 'micheal')
    res = _client.get('/get_book/'+ title)
    assert res.status_code == 200


def test_invalid_book(_client):
    """ valid book"""
    res = _client.get('/get_book/'+ 'title')
    assert res.status_code == 404
    assert res.data.decode('utf') == '{"msg":"not found"}\n'


def test_update_book(_client, __app):
    """Update book"""
    title = "test_book"
    __app.db.add_book(title, 'micheal')
    res = _client.put('/update_book/' + title, json={'title': title, 'author':'Michael_Kabali_Kisakyamukama'})
    assert res.status_code == 204

def test_delete_book(_client, __app):
    """Delete a book"""
    title = "test_book3"
    __app.db.add_book(title, 'michael')
    res = _client.delete('/delete_book/'+title)
    assert res.status_code == 202
