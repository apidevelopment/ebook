# # base image
# FROM tiangolo/uwsgi-nginx-flask:python3.7-alpine3.7

# ARG git_version
# ENV APP_VERSION=$git_version

# COPY ./requirements.txt /var/www/requirements.txt

# # install dependencies
# RUN apk add --no-cache --virtual=.build_dependencies musl-dev gcc python3-dev libffi-dev linux-headers && \
#     pip install -r /var/www/requirements.txt && \
#     rm -rf ~/.cache/pip && \
#     apk del .build_dependencies

# WORKDIR /var/www/app
# COPY . /var/www/app

# # CMD ["gunicorn", "-c", "gunicorn.py", "app.main.wsgi:app"]
# CMD ["gunicorn", "-w 4", "-b", ":5000", "app.main.wsgi:app"]

FROM python
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["/app/flask run"]
