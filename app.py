"""
    Main app
"""
from flask import Flask
from flask_json import FlaskJSON
from fake_db import FakeDB
from main_endpoints import main_bp


def create_app():
    """ Create app """
    app = Flask(__name__)
    FlaskJSON(app)
    app.db = FakeDB()

    app.register_blueprint(main_bp)
    return app
