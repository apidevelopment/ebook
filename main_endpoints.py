"""
    Route
"""

from flask import current_app as app
from flask import Blueprint, request
from flask_json import as_json

main_bp = Blueprint('main_bp', __name__)

@main_bp.route('/')
@as_json
def main():
    """ Simple route """
    return {}

@main_bp.route('/add_book', methods=['POST'])
@as_json
def add_book():
    """ Add book route"""
    try:
        title = request.get_json()['title']
        author = request.get_json()['author']
        if app.db.check_book(title, author):
            return {'message': 'exists'}
        app.db.add_book(title, author)
        return  {"message": "Book added successfully"}, 201
    except KeyError:
        return {'error': 'invalid'}, 401


@main_bp.route('/get_book/<title>', methods=['GET'])
def get_book(title):
    """get a book"""
    try:
        res = app.db.get_book(title)
        return {
            'title': title,
            'author': res[0]
        }, 200
    except KeyError:
        return {'msg': 'not found'}, 404

@main_bp.route('/update_book/<title>', methods=['PUT'])
def update_a_book(title):
    """ Update book"""
    try:
        new_author = request.get_json()['author']
        app.db.get_book(title)
        app.db.update_book(title, new_author)
        return {"message" : "Book updated successfully"}, 204
    except KeyError:
        return {"message": "Unable to update"}, 500

@main_bp.route('/delete_book/<title>', methods=['DELETE'])
def delete_book(title):
    """ delete a book """
    try:
        app.db.delete_book(title)
        return {'message':'Book deleted'}, 202
    except KeyError:
        return {"message": "Unable to delete book"}, 500
